;; Creates empty files in the current directory.
;; But I don't know why I can't create more that 15 files in a such way.
;; If you look in the debugger - all iteration executes properly.
segst segment para stack  'stack'
    db    64 dup('stack    ')
segst ends
dseg segment 'data'
    fileNameChar1 DB 0
    fileNameChar2 DB 0
    fileNameChar3 DB 0
    fileNameChar4 DB 0
    terminateChar DB 0
dseg ends
cseg segment
    assume ds:dseg,cs:cseg,ss:segst
main proc far
    push ds
    sub  AX, AX
    push AX
    mov  ax, dseg
    mov  ds, ax
    
    lea  dx, fileNameChar1
    mov  cx, 0
    
    mov  bx, 122                  ; char 'z'
    char1Changing:
        mov fileNameChar1, bl
        mov  ah, 3Ch        
        int  21h
        dec bx
            push bx
            mov  bx, 122
            char2Changing:
                mov fileNameChar2, bl
                mov  ah, 3Ch        
                int  21h
                dec bx
            cmp bx, 97
            jge char2Changing
            pop bx
    cmp bx, 97                    ; char 'a'
    jge char1Changing
    
    ret
main endp
cseg ends
end main